//
//  MockURLSession.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

import Foundation

class MockURLProtocol: URLProtocol {
    static var error: Error?
    static var data: Data?
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        if let data = MockURLProtocol.data {
            self.client?.urlProtocol(self, didLoad: data)
        }
        
        if let error = MockURLProtocol.error {
            self.client?.urlProtocol(self, didFailWithError: error)
        }
        self.client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() {}
}


