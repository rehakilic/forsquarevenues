//
//  MockLocationManager.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

@testable import ForsquareVenues
import MapKit

class MockLocationManager: LocationManager {
    private(set) var isAskForPermissionIfNeededCalled = false
    var lastLocation: CLLocation? = CLLocation(latitude: 52.37, longitude: 4.89)
    
    var delegate: LocationManagerDelegate?
    
    func askForPermissionIfNeeded() {
        isAskForPermissionIfNeededCalled = true
    }
}


