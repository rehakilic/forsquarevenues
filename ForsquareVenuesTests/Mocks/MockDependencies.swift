//
//  MockDependencies.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

@testable import ForsquareVenues
import UIKit
import CoreData

class MockDependencies: CoordinatorFactory, DependencyFactory {
    var venueStore: VenueStore { VenueStore(database: makeDatabase()) }
    
    func makeAppCoordinator(_ navigationController: UINavigationController) -> AppCoordinator {
        AppCoordinator(navigationController: UINavigationController(), factory: self)
    }
    
    func makeLocationManger() -> LocationManager { MockLocationManager() }
    
    func makeNetworkController() -> NetworkController { MockNetworkController(stubFile: .venues) }
        
    func makeDatabase() -> Database { CoreDataDatabase(configurationType: .inMemory) }
}

extension MockDependencies: VenuesSegmentBuilder {
    func build() -> VenuesSegmentViewController {
        VenuesSegmentViewController(factory: self)
    }
}

extension MockDependencies: NearbyVenuesBuilder {
    func build() -> NearbyVenuesViewController { NearbyVenuesViewController(presenter: MockNearbyVenuePresenter()) }
}
