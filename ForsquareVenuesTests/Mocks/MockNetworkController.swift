//
//  MockNetworkController.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

@testable import ForsquareVenues
import Foundation

class MockNetworkController: NetworkController {
    var status: Status = .success
    var session: URLSession = .shared
    var stubFile: StubFile
    
    init(stubFile: StubFile) {
        self.stubFile = stubFile
    }
    
    func get<T>(request: URLRequest, responseType: T.Type, completion: @escaping ((Swift.Result<T, NetworkError>) -> Void)) where T : Decodable {
        switch status {
        case .success:
            if let dataModel = try? JSONDecoder().decode(T.self, from: stubbedResponse(from: stubFile)) {
                completion(.success(dataModel))
            } else {
                completion(.failure(.unableToParse))
            }
        case .failure:
            completion(.failure(.somethingWentWrong))
        case .unableToParse:
            completion(.failure(.unableToParse))
        }
    }
    
    enum Status {
        case success, failure, unableToParse
    }
}
