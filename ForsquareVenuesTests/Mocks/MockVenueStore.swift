//
//  MockVenueStore.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

//@testable import ForsquareVenues
//
//class MockVenueStore: VenueStore {
//    private(set) var saveChangesCalled = false
//    private(set) var deletedEntities: [VenueEntity] = []
//    private(set) var createdEntity = false
//
//    override func saveChanges() {
//        self.saveChangesCalled = true
//    }
//
//    override func delete(_ item: VenueEntity) {
//        self.deletedEntities.append(item)
//    }
//
//    override func fetchAll(completion: ([VenueEntity]) -> Void) {
//        if let savedEntity = savedEntity {
//            completion([savedEntity])
//        } else {
//            completion([])
//        }
//    }
//    
//    override func create(using dataModel: ForsquareDataModel) {
//        
//    }
//}
