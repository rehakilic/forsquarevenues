//
//  MockNetworkControllerTests.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

import XCTest
@testable import ForsquareVenues

class NetworkControllerTests: XCTestCase {
    var sut: NetworkController!
    
    override func setUp() {
        super.setUp()
        MockURLProtocol.data = stubbedResponse(from: .venues)
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]
        let session = URLSession(configuration: configuration)
        sut = DefaultNetworkController(session: session)
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_ShouldReturnResponseWhenFetchSucceeds() {
        // Given
        let exp = expectation(description: "Fetch Expectation")
        var response: ForsquareDataModel?
        
        // When
        let urlRequest = URLRequest(url: URL(fileURLWithPath: ""))
        sut.get(request: urlRequest,
                responseType: ForsquareDataModel.self) { result in
            switch result {
            case .success(let resp):
                response = resp
            case .failure:
                XCTAssertTrue(false)
            }
            exp.fulfill()
        }
        
        // Then
        wait(for: [exp], timeout: 1.0)
        XCTAssertNotNil(response)
    }
    
    func test_ShouldReturnSomethingWentWrongWhenFetchesWithError() {
        // Given
        MockURLProtocol.error = NSError(domain: "", code: 400, userInfo: nil)
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]
        let session = URLSession(configuration: configuration)
        sut = DefaultNetworkController(session: session)
        
        let exp = expectation(description: "Fetch Expectation")
        var error: NetworkError?
        
        // When
        let urlRequest = URLRequest(url: URL(fileURLWithPath: ""))
        sut.get(request: urlRequest,
                responseType: ForsquareDataModel.self) { result in
            switch result {
            case .success:
                XCTAssertTrue(false)
            case .failure(let err):
                error = err
            }
            exp.fulfill()
        }
        
        // Then
        wait(for: [exp], timeout: 1.0)
        XCTAssertEqual(error, NetworkError.somethingWentWrong)
    }
    
    func test_ShouldReturnParseErrorWhenFetchesInvalidData() {
        // Given
        MockURLProtocol.data = Data()
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]
        let session = URLSession(configuration: configuration)
        sut = DefaultNetworkController(session: session)
        
        let exp = expectation(description: "Fetch Expectation")
        var error: NetworkError?
        
        // When
        let urlRequest = URLRequest(url: URL(fileURLWithPath: ""))
        sut.get(request: urlRequest,
                responseType: ForsquareDataModel.self) { result in
            switch result {
            case .success:
                XCTAssertTrue(false)
            case .failure(let err):
                error = err
            }
            exp.fulfill()
        }
        
        // Then
        wait(for: [exp], timeout: 1.0)
        XCTAssertEqual(error, NetworkError.unableToParse)
    }
}

