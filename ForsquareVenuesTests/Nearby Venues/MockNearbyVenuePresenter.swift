//
//  MockNearbyVenuePresenter.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

@testable import ForsquareVenues

class MockNearbyVenuePresenter: NearbyVenuesPresenterProtocol {
    private(set) var viewDidLoadCalled = false
    
    func viewDidLoad() {
        self.viewDidLoadCalled = true
    }
}
