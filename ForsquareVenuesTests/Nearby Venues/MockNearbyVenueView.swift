//
//  MockNearbyVenueView.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

@testable import ForsquareVenues

class MockNearbyVenueView: NearbyVenuesViewProtocol {
    private(set) var showingAuthorizationStatus = false
    private(set) var showingAnnotations: [Annotation] = []
    private(set) var updatedUserLocation: (Double, Double) = (0,0)
    private(set) var showFetchErrorCalled = false
    
    func showAuthorizationStatus(authorized: Bool) {
        self.showingAuthorizationStatus = authorized
    }
    
    func showAnnotations(_ annotations: [Annotation]) {
        self.showingAnnotations = annotations
    }
    
    func showFetchError() {
        showFetchErrorCalled = true
    }
    
    func updateUserLocation(_ location: (Double, Double)) {
        self.updatedUserLocation = location
    }
}
