//
//  NearbyVenuesPresenterTests.swift
//  ForsquareVenuesTests
//
//  Created by Reha Kılıç on 16.02.2022.
//

import XCTest
@testable import ForsquareVenues

class NearbyVenuesPresenterTests: XCTestCase {
    var sut: NearbyVenuesPresenter!
    var networkController: NetworkController!
    var locationManager: LocationManager!
    var venueStore: VenueStore!
    var mockNearbyVenueView: MockNearbyVenueView!
    
    override func setUp() {
        super.setUp()
        let mockDependencies = MockDependencies()
        locationManager = mockDependencies.makeLocationManger()
        networkController = mockDependencies.makeNetworkController()
        venueStore = mockDependencies.venueStore
        
        sut = NearbyVenuesPresenter(locationManager: locationManager,
                                    networkController: networkController,
                                    venueStore: venueStore)
        self.mockNearbyVenueView = MockNearbyVenueView()
        sut.view = mockNearbyVenueView
    }

    override func tearDown() {
        sut = nil
        networkController = nil
        locationManager = nil
        venueStore = nil
        mockNearbyVenueView = nil
        super.tearDown()
    }
    
    func test_ShouldAskForPermissionOnViewDidLoad() {
        // when
        sut.viewDidLoad()
        
        // Then
        let mockLocationManager = locationManager as! MockLocationManager
        XCTAssertTrue(mockLocationManager.isAskForPermissionIfNeededCalled)
    }
    
    func test_ShouldShowAuthorizationStatusWhenDidChange() {
        // Given
        let authorized = true
        
        // When
        sut.locationManager(locationManager, didChangeAuthorization: authorized)
        
        // Then
        let mockView = sut.view as! MockNearbyVenueView
        XCTAssertEqual(mockView.showingAuthorizationStatus, authorized)
    }
    
    func test_ShouldUpdateUserLocationWhenDidUpdateLocation() {
        // Given
        let location = (4.0, 5.0)
        
        // When
        sut.locationManager(locationManager, didUpdate: location, isSignificantDistance: false)
        
        // Then
        let mockView = sut.view as! MockNearbyVenueView
        XCTAssertEqual(mockView.updatedUserLocation.0, location.0)
        XCTAssertEqual(mockView.updatedUserLocation.1, location.1)
    }
    
    func test_ShouldFetchVenuesAndSaveWhenDidUpdateSignificantDistance() {
        // When
        sut.locationManager(locationManager, didUpdate: (4.0, 10.0), isSignificantDistance: true)
        
        // Then
        let mockView = sut.view as! MockNearbyVenueView
        XCTAssertFalse(mockView.showingAnnotations.isEmpty)
        
        venueStore.fetchAll { entities in
            XCTAssertFalse(entities.isEmpty)
        }
    }
    
    func test_ShouldFetchVenuesFromDatabaseAndShowNetworkErrorIfReceivesNetworkFailure() {
        // Given
        sut.locationManager(locationManager, didUpdate: (5.2, 2.3), isSignificantDistance: true) // will save to db
        (networkController as? MockNetworkController)?.status = .failure
        
        // When
        sut.locationManager(locationManager, didUpdate: (25.2, 12.3), isSignificantDistance: true)
        
        // Then
        let mockView = sut.view as! MockNearbyVenueView
        XCTAssertFalse(mockView.showingAnnotations.isEmpty)
        XCTAssertTrue(mockView.showFetchErrorCalled)
    }
    
    func test_ShouldDeleteEntitesAndStoreDatabase() {
        // Given
        sut.locationManager(locationManager, didUpdate: (5.2, 2.3), isSignificantDistance: true) // will save to db
        
        var count = 0
        venueStore.fetchAll { entities in
            count = entities.count
        }
        
        // When
        sut.locationManager(locationManager, didUpdate: (25.2, 12.3), isSignificantDistance: true)
        
        // Then
        venueStore.fetchAll { entities in
            XCTAssertEqual(entities.count, count)
        }
    }
}


