//
//  CoreDataDatabase.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import Foundation
import UIKit
import CoreData

final class CoreDataDatabase: Database {
    private let configurationType: CoreDataConfiguration
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))] )!
        return managedObjectModel
    }()
    lazy var inMemoryPersistantContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ForsquareVenues", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false // Make it simpler in test env
        
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            
            precondition( description.type == NSInMemoryStoreType )
            
            if let error = error {
                fatalError("Create an in memort store failed \(error)")
            }
        }
        return container
    }()
    lazy var basicPersistantContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ForsquareVenues")
        container.loadPersistentStores { (description, error) in
            if let error = error {
                fatalError("Create an basic store failed \(error)")
            }
        }
        return container
    }()
    
    
    init(configurationType: CoreDataConfiguration) {
        self.configurationType = configurationType
    }
    
    func saveChanges() throws {
        do {
            try getManagedContext().save()
        } catch let error {
            print("Could not save. \(error.localizedDescription)")
        }
    }
    
    func delete(entity: Entity) throws {
        guard let object = entity as? NSManagedObject else { throw CoreDataError.castingError }
        getManagedContext().delete(object)
        do {
            try getManagedContext().save()
        } catch let error {
            print("Could not delete. \(error.localizedDescription)")
        }
    }
    
    func fetch<T>(_ type: T.Type, completion: (([T]) -> Void)) where T : Entity {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: T.self))
        do {
            let objects = try getManagedContext().fetch(request)
            var entities: [T] = []
            for object in objects {
                if let object = object as? T {
                    entities.append(object)
                }
            }
            completion(entities)
        } catch let error {
            print("Could not fetch. \(error.localizedDescription)")
        }
    }
}

private extension CoreDataDatabase {
    func getManagedContext() -> NSManagedObjectContext {
        switch self.configurationType {
        case .basic:
            return basicPersistantContainer.viewContext
        case .inMemory:
            return inMemoryPersistantContainer.viewContext
        }
    }
}

enum CoreDataConfiguration {
    case basic, inMemory
}

enum CoreDataError: Error {
    case castingError
}

extension NSManagedObject: Entity {}

extension CoreDataDatabase {
    func create(using dataModel: ForsquareDataModel) {
        for result in dataModel.results {
            let venueEntity = VenueEntity(context: getManagedContext())
            venueEntity.id = result.fsqID
            venueEntity.address = result.location.address
            venueEntity.categories = result.categories.map {$0.name}
            venueEntity.lat = result.geocodes.main.latitude
            venueEntity.lon = result.geocodes.main.longitude
            venueEntity.name = result.name
            venueEntity.distance = result.distance
        }
    }
}
