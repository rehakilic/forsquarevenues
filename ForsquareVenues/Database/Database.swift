//
//  Database.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import Foundation

protocol Entity {}

protocol Database {
    func save(entity: Entity) throws
    func delete(entity: Entity) throws
    func saveChanges() throws
    func fetch<T: Entity>(_ type: T.Type, completion: (([T]) -> Void))
}

extension Database {
    func save(entity: Entity) throws {}
    func saveChanges() throws {}
}
