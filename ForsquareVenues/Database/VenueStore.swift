//
//  VenueStore.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import Foundation

protocol StoreProtocol {
    associatedtype Item
    func save(_ item: Item)
    func saveChanges()
    func delete(_ item: Item)
    func fetchAll(completion: ([Item]) -> Void)
}

extension StoreProtocol {    
    func save(_ item: Item) {}
    func saveChanges() {}
}

class VenueStore: StoreProtocol {
    typealias Item = VenueEntity
    private let database: Database
    
    init(database: Database) {
        self.database = database
    }
    
    func saveChanges() {
        try? database.saveChanges()
    }
    
    func delete(_ item: VenueEntity) {
        try? database.delete(entity: item)
    }
    
    func fetchAll(completion: ([VenueEntity]) -> Void) {
        database.fetch(VenueEntity.self) {completion($0)}
    }
}

extension VenueStore {    
    func create(using dataModel: ForsquareDataModel) {
        if let coreDataDatabase = database as? CoreDataDatabase {
            coreDataDatabase.create(using: dataModel)
        }
    }
}
