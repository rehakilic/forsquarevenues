//
//  AppCoordinator.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import UIKit

protocol Coordinator {
    var children: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    func start()
}

class AppCoordinator: Coordinator {
    var navigationController: UINavigationController
    var children: [Coordinator] = []
    typealias Factory = VenuesSegmentBuilder
    private let factory: Factory
    
    init(navigationController: UINavigationController, factory: Factory) {
        self.navigationController = navigationController
        self.factory = factory
    }
    
    func start() {
        navigationController.viewControllers = [factory.build()]
    }
}

