//
//  LocationManager.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import MapKit

protocol LocationManagerDelegate: AnyObject {
    func locationManager(_ locationManager: LocationManager, didChangeAuthorization authorized: Bool)
    func locationManager(_ locationManager: LocationManager, didUpdate location: (Double, Double), isSignificantDistance: Bool)
}

protocol LocationManager {
    var lastLocation: CLLocation? { get set }
    var delegate: LocationManagerDelegate? { get set }
    func askForPermissionIfNeeded()
}

final class DefaultLocationManager: NSObject, LocationManager {
    weak var delegate: LocationManagerDelegate?
    lazy var manager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        return manager
    }()
    
    var isLocationPermissionGranted: Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch manager.authorizationStatus {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            @unknown default:
                return false
            }
        } else {
            return false
        }
    }
    var lastLocation: CLLocation?
    
    func askForPermissionIfNeeded() {
        if !isLocationPermissionGranted {
            manager.requestWhenInUseAuthorization()
        } else {
            manager.startUpdatingLocation()
        }
    }
}

extension DefaultLocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        let authorized = status == .authorizedAlways || status == .authorizedWhenInUse
        if authorized { manager.startUpdatingLocation() }
        delegate?.locationManager(self, didChangeAuthorization: authorized)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last else { return }
        let location = (lastLocation.coordinate.latitude, lastLocation.coordinate.longitude)
        
        let prevLocation = self.lastLocation ?? CLLocation(latitude: 0, longitude: 0)
        let distance = lastLocation.distance(from: prevLocation)
        
        delegate?.locationManager(self, didUpdate: location, isSignificantDistance: distance > 100)
        self.lastLocation = lastLocation
    }
}
