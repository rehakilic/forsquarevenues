//
//  AnnotationView.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import UIKit

final class AnnotationView: UIView {
    lazy var titleLabel = makeBoldLabel()
    lazy var addressLabel = makeLightLabel()
    lazy var distanceLabel = makeLightLabel()
    lazy var localNameLabel = makeMediumLabel()
    lazy var categoriesLabel = makeMediumLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension AnnotationView {
    func buildSubviews() {
        let infoStackView = UIStackView(arrangedSubviews: [distanceLabel, localNameLabel, addressLabel])
        infoStackView.distribution = .equalCentering
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, categoriesLabel, infoStackView])
        stackView.axis = .vertical
        stackView.spacing = 4
        
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        let stackViewConstraints = [
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.widthAnchor.constraint(lessThanOrEqualToConstant: 300)
        ]

        
        NSLayoutConstraint.activate(stackViewConstraints)
    }
    
    func makeBoldLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        return label
    }
    
    func makeMediumLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        return label
    }
    
    func makeLightLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        return label
    }

}


