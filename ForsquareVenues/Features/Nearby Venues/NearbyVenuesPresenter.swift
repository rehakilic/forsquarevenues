//
//  NearbyVenuesPresenter.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import Foundation

protocol NearbyVenuesPresenterProtocol {
    func viewDidLoad()
}

final class NearbyVenuesPresenter: NearbyVenuesPresenterProtocol {
    weak var view: NearbyVenuesViewProtocol?
    private var locationManager: LocationManager
    private let networkController: NetworkController
    private let venueStore: VenueStore
    
    init(locationManager: LocationManager,
         networkController: NetworkController,
         venueStore: VenueStore) {
        self.locationManager = locationManager
        self.networkController = networkController
        self.venueStore = venueStore
        self.locationManager.delegate = self
    }
    
    func viewDidLoad() {
        locationManager.askForPermissionIfNeeded()
    }
}

private extension NearbyVenuesPresenter {
    func fetchVenues(lat: Double, lon: Double) {
        guard let url = URL(string: "https://api.foursquare.com/v3/places/nearby?ll=\(lat)%2C\(lon)&limit=5") else { return }
        let headers = [
            "Accept": "application/json",
            "Authorization": "fsq3DgHXgEn9Rkctx6jSXvqI9y0CA8H0DcWrPTe95mCPAzk="
        ]
        
        var request = URLRequest(url: url, timeoutInterval: 10.0)
        request.allHTTPHeaderFields = headers
        networkController.get(request: request, responseType: ForsquareDataModel.self) { [weak self] result in
            switch result {
            case .success(let dataModel):
                self?.view?.showAnnotations(Annotation.create(from: dataModel))
                self?.storeToDatabase(dataModel: dataModel)
            case .failure:
                self?.fetchFromDatabase()
                self?.view?.showFetchError()
            }
        }
    }
    
    func storeToDatabase(dataModel: ForsquareDataModel) {
        venueStore.fetchAll { existingEntities in
            existingEntities.forEach {venueStore.delete($0)}
            venueStore.create(using: dataModel)
            venueStore.saveChanges()
        }
    }
    
    func fetchFromDatabase() {
        venueStore.fetchAll { [weak self] venueEntities in
            self?.view?.showAnnotations(Annotation.create(from: venueEntities))
        }
    }
}

extension NearbyVenuesPresenter: LocationManagerDelegate {
    func locationManager(_ locationManager: LocationManager, didChangeAuthorization authorized: Bool) {
        view?.showAuthorizationStatus(authorized: authorized)
    }
    
    func locationManager(_ locationManager: LocationManager, didUpdate location: (Double, Double), isSignificantDistance: Bool) {
        view?.updateUserLocation(location)
        if isSignificantDistance {
            fetchVenues(lat: location.0, lon: location.1)
        }
    }
}
