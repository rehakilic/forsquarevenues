//
//  NearbyVenuesViewController.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import UIKit
import MapKit

protocol NearbyVenuesViewProtocol: AnyObject {
    func showAuthorizationStatus(authorized: Bool)
    func updateUserLocation(_ location: (Double, Double))
    func showAnnotations(_ annotations: [Annotation])
    func showFetchError()
}

final class NearbyVenuesViewController: UIViewController {
    private let nearbyVenuesView = NearbyVenuesView()
    private let presenter: NearbyVenuesPresenterProtocol
    private var annotations: [String: Annotation] = [:]

    init(presenter: NearbyVenuesPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
   
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = nearbyVenuesView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        presenter.viewDidLoad()
    }
}

private extension NearbyVenuesViewController {
    func configure() {
        nearbyVenuesView.mapView.delegate = self
        nearbyVenuesView.mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: "Annotation")
    }
}

extension NearbyVenuesViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "Annotation") as? MKMarkerAnnotationView
        annotationView?.canShowCallout = true
        let id = "\(annotation.coordinate.latitude),\(annotation.coordinate.longitude)"
        guard let ann = self.annotations[id] else { return MKAnnotationView() }
        
        let customView = AnnotationView()
        customView.titleLabel.text = ann.name
        customView.addressLabel.text = ann.address
        customView.categoriesLabel.text = ann.categories.joined(separator: " / ")
        customView.localNameLabel.text = ann.name
        customView.distanceLabel.text = "\(ann.distance.rounded()) m"
        annotationView?.detailCalloutAccessoryView = customView
        return annotationView
    }
}

extension NearbyVenuesViewController: NearbyVenuesViewProtocol {
    func showAuthorizationStatus(authorized: Bool) {
        nearbyVenuesView.errorLabel.text = authorized ? "" : "Location services are disabled"
    }
    
    func updateUserLocation(_ location: (Double, Double)) {
        let center = CLLocationCoordinate2D(latitude: location.0, longitude: location.1)
        let region = MKCoordinateRegion(center: center, latitudinalMeters: 100, longitudinalMeters: 100)
        nearbyVenuesView.mapView.setRegion(region, animated: true)
    }
    
    func showAnnotations(_ annotations: [Annotation]) {
        nearbyVenuesView.errorLabel.text = ""
        nearbyVenuesView.mapView.removeAnnotations(nearbyVenuesView.mapView.annotations)
        self.annotations.removeAll()
        
        let pointAnnotations: [MKPointAnnotation] = annotations.map {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: $0.lat,
                                                           longitude: $0.lon)
            return annotation
        }
        
        
        annotations.forEach {
            let id = "\($0.lat),\($0.lon)"
            self.annotations[id] = $0
        }
        nearbyVenuesView.mapView.addAnnotations(pointAnnotations)
    }
    
    func showFetchError() {
        nearbyVenuesView.errorLabel.text = "An error occured when fetching"
    }
}
