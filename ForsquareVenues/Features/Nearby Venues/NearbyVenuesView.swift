//
//  NearbyVenuesView.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import MapKit
import UIKit

final class NearbyVenuesView: UIView {
    lazy var errorLabel = makeErrorLabel()
    lazy var mapView = makeMapView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension NearbyVenuesView {
    func buildSubviews() {
        addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        let mapViewConstraints = [
            mapView.topAnchor.constraint(equalTo: topAnchor),
            mapView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
                
        addSubview(errorLabel)
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        let errorLabelConstraints = [
            errorLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            errorLabel.leadingAnchor.constraint(equalTo: mapView.leadingAnchor),
            errorLabel.trailingAnchor.constraint(equalTo: mapView.trailingAnchor)
        ]
        
        NSLayoutConstraint.activate(mapViewConstraints)
        NSLayoutConstraint.activate(errorLabelConstraints)
    }
    
    func makeErrorLabel() -> UILabel {
        let label = UILabel()
        label.textColor = .red
        label.textAlignment = .center
        return label
    }
    
    
    func makeMapView() -> MKMapView {
        let mapView = MKMapView()
        mapView.showsUserLocation = true
        return mapView
    }
}

