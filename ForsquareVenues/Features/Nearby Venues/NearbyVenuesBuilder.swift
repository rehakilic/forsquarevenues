//
//  NearbyVenuesBuilder.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

protocol NearbyVenuesBuilder {
    func build() -> NearbyVenuesViewController
}

extension Dependencies: NearbyVenuesBuilder {
    func build() -> NearbyVenuesViewController {
        let presenter = NearbyVenuesPresenter(locationManager: makeLocationManger(),
                                              networkController: makeNetworkController(),
                                              venueStore: venueStore)
        let controller = NearbyVenuesViewController(presenter: presenter)
        presenter.view = controller
        return controller
    }
}

