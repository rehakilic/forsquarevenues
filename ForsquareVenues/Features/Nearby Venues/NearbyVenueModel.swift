//
//  NearbyVenueModel.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import Foundation

struct Annotation {
    let id: String
    let name: String
    let lat: Double
    let lon: Double
    let address: String
    let distance: Double
    let categories: [String]
    
    static func create(from response: ForsquareDataModel) -> [Annotation] {
        return response.results.map { result in
            let categories = result.categories.map {$0.name}
            return Annotation(id: result.fsqID,
                              name: result.name,
                              lat: result.geocodes.main.latitude,
                              lon: result.geocodes.main.longitude,
                              address: result.location.address ?? "",
                              distance: result.distance,
                              categories: categories)
        }
    }
    
    static func create(from entities: [VenueEntity]) -> [Annotation] {
        return entities.map { entity in
            return Annotation(id: entity.id ?? "",
                              name: entity.name ?? "",
                              lat: entity.lat,
                              lon: entity.lon,
                              address: entity.address ?? "",
                              distance: entity.distance,
                              categories: entity.categories ?? [])
        }
    }
}

extension Annotation: Entity {}
