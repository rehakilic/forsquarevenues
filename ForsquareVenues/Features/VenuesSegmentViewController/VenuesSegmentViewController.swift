//
//  VenuesSegmentViewController.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 16.02.2022.
//

import Foundation
import UIKit

final class VenuesSegmentViewController: UIViewController {
    private let segmentView = VenuesSegmentView()
    private var displayingViewController: UIViewController?
    private lazy var nearbyVenuesViewController = makeNearbyVenuesViewController()
    private lazy var aboutUsViewController = makeAboutUsViewController()
    typealias Factory = NearbyVenuesBuilder
    private let factory: Factory
    
    init(factory: Factory) {
        self.factory = factory
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = segmentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentView.segmentedControl.selectedSegmentIndex = 0
        segmentView.segmentedControl.addTarget(self, action: #selector(segmentChanged(_:)), for: .valueChanged)        
        segmentChanged(segmentView.segmentedControl)
    }
}

private extension VenuesSegmentViewController {
    @objc
    func segmentChanged(_ sender: UISegmentedControl) {
        guard let selectedSegment = VenueSegment(rawValue: sender.selectedSegmentIndex) else { return }
        switch selectedSegment {
        case .nearbyVenues:
            replace(with: nearbyVenuesViewController)
        case .aboutUs:
            replace(with: aboutUsViewController)
        }
    }
    
    func replace(with viewController: UIViewController) {
        if let displayingViewController = displayingViewController {
            guard displayingViewController != viewController else { return }
            displayingViewController.remove()
        }
        add(viewController, to: segmentView.containerView)
        displayingViewController = viewController
    }
}

private extension VenuesSegmentViewController {
    func makeNearbyVenuesViewController() -> NearbyVenuesViewController { factory.build() }
    func makeAboutUsViewController() -> AboutUsViewController { AboutUsViewController() }
}

enum VenueSegment: Int, CaseIterable {
    case nearbyVenues = 0, aboutUs
    
    var name: String {
        switch self {
        case .nearbyVenues:
            return "Nearby Venues"
        case .aboutUs:
            return "About Us"
        }
    }
}
