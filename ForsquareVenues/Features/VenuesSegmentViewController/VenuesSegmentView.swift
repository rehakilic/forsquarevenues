//
//  VenuesSegmentView.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 16.02.2022.
//

import UIKit

final class VenuesSegmentView: UIView {
    lazy var segmentedControl = makeSegmentControl()
    let containerView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        buildSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension VenuesSegmentView {
    func buildSubviews() {
        addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        let containerViewConstraints = [
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
                
        addSubview(segmentedControl)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        let segmentedControlConstraints = [
            segmentedControl.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            segmentedControl.centerXAnchor.constraint(equalTo: centerXAnchor),
            segmentedControl.widthAnchor.constraint(equalToConstant: 250)
        ]
        
        NSLayoutConstraint.activate(containerViewConstraints)
        NSLayoutConstraint.activate(segmentedControlConstraints)
    }
    
    
    func makeSegmentControl() -> UISegmentedControl {
        let items = VenueSegment.allCases.map { $0.name }
        let segmentedControl = UISegmentedControl(items: items)
        return segmentedControl
    }
}


