//
//  VenuesSegmentBuilder.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 16.02.2022.
//

protocol VenuesSegmentBuilder {
    func build() -> VenuesSegmentViewController
}

extension Dependencies: VenuesSegmentBuilder {
    func build() -> VenuesSegmentViewController { VenuesSegmentViewController(factory: self) }
}


