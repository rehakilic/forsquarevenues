//
//  AboutUsViewController.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 16.02.2022.
//

import Foundation
import UIKit

final class AboutUsViewController: UIViewController {
    lazy var textView = makeTextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTextView()
    }
}

private extension AboutUsViewController {
    func addTextView() {
        view.addSubview(textView)
        textView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 28),
            textView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            textView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    func makeTextView() -> UITextView {
        let textView = UITextView()
        textView.text = "ABOUT US"
        textView.isEditable = false
        return textView
    }
}
