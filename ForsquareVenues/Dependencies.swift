//
//  Dependencies.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import Foundation
import UIKit
import CoreData

struct Dependencies {
    var venueStore: VenueStore { VenueStore(database: makeDatabase()) }
}

protocol CoordinatorFactory {
    func makeAppCoordinator(_ navigationController: UINavigationController) -> AppCoordinator
}

extension Dependencies: CoordinatorFactory {
    func makeAppCoordinator(_ navigationController: UINavigationController) -> AppCoordinator {
        AppCoordinator(navigationController: navigationController, factory: self)
    }
}

protocol DependencyFactory {
    func makeLocationManger() -> LocationManager
    func makeNetworkController() -> NetworkController
    func makeDatabase() -> Database
}

extension Dependencies: DependencyFactory {    
    func makeLocationManger() -> LocationManager { DefaultLocationManager() }
    func makeNetworkController() -> NetworkController { DefaultNetworkController() }
    func makeDatabase() -> Database { CoreDataDatabase(configurationType: .basic) }
}

