//
//  ForsquareDataModel.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import Foundation

// MARK: - ForsquareDataModel
struct ForsquareDataModel: Decodable {
    let results: [Result]
}

// MARK: - Result
struct Result: Decodable {
    let fsqID: String
    let categories: [Category]
    let distance: Double
    let geocodes: Geocodes
    let location: Location
    let name: String
    let relatedPlaces: RelatedPlaces
    let timezone: String

    enum CodingKeys: String, CodingKey {
        case fsqID = "fsq_id"
        case categories, distance, geocodes, location, name
        case relatedPlaces = "related_places"
        case timezone
    }
}

// MARK: - Category
struct Category: Decodable {
    let id: Int
    let name: String
    let icon: Icon
}

// MARK: - Icon
struct Icon: Decodable {
    let iconPrefix: String
    let suffix: String

    enum CodingKeys: String, CodingKey {
        case iconPrefix = "prefix"
        case suffix
    }
}

// MARK: - Geocodes
struct Geocodes: Decodable {
    let frontDoor: FrontDoor?
    let main: FrontDoor

    enum CodingKeys: String, CodingKey {
        case frontDoor = "front_door"
        case main
    }
}

// MARK: - FrontDoor
struct FrontDoor: Decodable {
    let latitude, longitude: Double
}

// MARK: - Location
struct Location: Decodable {
    let address: String?
}


// MARK: - RelatedPlaces
struct RelatedPlaces: Decodable {
    let children: [Parent]?
    let parent: Parent?
}

// MARK: - Parent
struct Parent: Decodable {
    let fsqID, name: String

    enum CodingKeys: String, CodingKey {
        case fsqID = "fsq_id"
        case name
    }
}
