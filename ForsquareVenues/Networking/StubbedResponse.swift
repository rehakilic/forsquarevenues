//
//  StubbedResponse.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 16.02.2022.
//

import Foundation

enum StubFile: String {
    case venues
}

func stubbedResponse(from stubFile: StubFile) -> Data {
    let file = File(name: stubFile.rawValue, extension: "json")
    guard let data = read(file) else {
        assertionFailure("Path not found for filename: \(file.name)")
        return Data()
    }
    return data
}

func read(_ file: File) -> Data? {
    class TestClass: NSObject { }
    let filename = file.name
    let bundle = Bundle(for: TestClass.self)
    guard let path = bundle.path(forResource: filename, ofType: file.extension) else {
        return nil
    }
    guard let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
        return nil
    }
    return data
}

struct File: CustomDebugStringConvertible {
    let name: String
    let `extension`: String

    var debugDescription: String {
        return self.name + "." + self.extension
    }
}
