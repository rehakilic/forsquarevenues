//
//  NetworkController.swift
//  ForsquareVenues
//
//  Created by Reha Kılıç on 15.02.2022.
//

import Foundation

protocol NetworkController {
    var session: URLSession { get }
    func get<T: Decodable>(request: URLRequest,
                           responseType: T.Type,
                           completion: @escaping ((Swift.Result<T, NetworkError>) -> Void))
}

final class DefaultNetworkController: NetworkController {
    private(set) var session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func get<T: Decodable>(request: URLRequest,
                           responseType: T.Type,
                           completion: @escaping ((Swift.Result<T, NetworkError>) -> Void)) {
        session.dataTask(with: request) { data, resp, err in
            guard let data = data,
                  err == nil else {
                      DispatchQueue.main.async { completion(.failure(.somethingWentWrong)) }
                      return
                  }
            do {
                let dataModel = try JSONDecoder().decode(T.self, from: data)
                DispatchQueue.main.async { completion(.success(dataModel)) }
            } catch let error {
                print(error)
                DispatchQueue.main.async { completion(.failure(.unableToParse)) }
            }
        }.resume()
    }
}

enum NetworkError: Error, Equatable {
    case somethingWentWrong
    case unableToParse
}
